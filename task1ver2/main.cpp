#include <iostream>

//using namespace std;
class Dog;
class Cat;
class Animal{
public:
    virtual void meetVoice(Cat* animal) = 0;
    virtual void meetVoice(Dog* animal) = 0;
    static void meeting(Animal*, Animal*);
};


class Dog : public Animal{
public:
    virtual void meetVoice(Cat* animal) override{
        if(animal)
            std::cout << "Bark Meow" << std::endl;
    }
    virtual void meetVoice(Dog* animal) override{
        if(animal)
            std::cout << "Woof-Woof" << std::endl;
    }

};

class Cat : public Animal{
public:
    virtual void meetVoice(Dog* animal) override{
        if(animal)
            std::cout << "Meow Bark" << std::endl;
    }
    virtual void meetVoice(Cat* animal) override{
        if(animal)
            std::cout << "Purr Purr" << std::endl;
    }
};

void Animal::meeting(Animal* a, Animal* b){
    Dog* dog = dynamic_cast<Dog*>(a);
    if(dog){
        dog->meetVoice(dynamic_cast<Dog*>(b));
        dog->meetVoice(dynamic_cast<Cat*>(b));
    }
    Cat* cat = dynamic_cast<Cat*>(a);
    if(cat){
        cat->meetVoice(dynamic_cast<Dog*>(b));
        cat->meetVoice(dynamic_cast<Cat*>(b));
    }
}

int main()
{
    Animal* a = new Dog();
    Animal* b = new Cat();
    Animal::meeting(a, a);
    Animal::meeting(a, b);
    Animal::meeting(b, a);
    Animal::meeting(b, b);

    return 0;
}
